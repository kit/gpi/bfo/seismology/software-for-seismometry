.
├── [4.0K]  software/
│   ├── [4.0K]  calex/
│   │   ├── [4.0K]  data/
│   │   │   └── [ 52K]  rtein.f
│   │   ├── [ 393]  calex01.par
│   │   ├── [ 46K]  calex10.f
│   │   ├── [ 393]  calex1.par
│   │   ├── [ 372]  calex20.par
│   │   ├── [ 372]  calex2.par
│   │   ├── [ 66K]  calex.doc
│   │   ├── [ 61K]  calex.f
│   │   ├── [ 61K]  CALEX.F
│   │   ├── [ 372]  calex.par
│   │   ├── [1.3K]  CALEX-PZ.PAR
│   │   ├── [178K]  EX_5.4.doc
│   │   ├── [176K]  EX_5.4_rev1.doc
│   │   ├── [ 122]  plot3.bat
│   │   ├── [ 43K]  qcalex.f
│   │   ├── [ 25K]  trical.doc
│   │   ├── [ 50K]  trical.f
│   │   ├── [ 506]  trical.par
│   │   └── [ 388]  two-tone.par
│   ├── [4.0K]  dispcal/
│   │   ├── [ 45K]  dispcal.doc
│   │   ├── [ 43K]  dispcal.f
│   │   ├── [ 909]  dispcal.par
│   │   ├── [ 38K]  dispcalx3.f
│   │   ├── [ 873]  dispcalx3.par
│   │   └── [ 552]  winplot.par
│   ├── [4.0K]  ems/
│   │   ├── [ 11K]  ems.g32
│   │   ├── [ 287]  ems.par
│   │   ├── [ 287]  GS13.par
│   │   ├── [ 287]  GS-1.par
│   │   └── [ 287]  Mark-L4.par
│   ├── [4.0K]  filtdemo/
│   │   ├── [ 22K]  FILTDEMO.doc
│   │   └── [ 19K]  Filtdemo.g32
│   ├── [4.0K]  fourierdemo/
│   │   └── [3.0K]  Fourierdemo.g32
│   ├── [4.0K]  input/
│   │   ├── [1.1K]  demux.f
│   │   ├── [4.1K]  input.f
│   │   ├── [5.4K]  multinput.f
│   │   ├── [ 996]  output.f
│   │   ├── [5.1K]  ReadTaurus.G32
│   │   └── [2.3K]  sg2convert.g32
│   ├── [4.0K]  inverseif/
│   │   ├── [1.6K]  gauss.par
│   │   ├── [ 22K]  inverseif.doc
│   │   ├── [ 35K]  inverseif.f
│   │   └── [1.6K]  inverseif.par
│   ├── [4.0K]  inversion/
│   │   ├── [1.9K]  invers1.par
│   │   ├── [2.0K]  invers2.par
│   │   ├── [ 23K]  invers.doc
│   │   ├── [ 19K]  invers.f
│   │   └── [1.9K]  invers.par
│   ├── [4.0K]  leafspring/
│   │   ├── [ 24K]  Adjusting-a-leafspring.doc
│   │   ├── [3.7K]  heli.f
│   │   ├── [ 19K]  leaffree.f
│   │   ├── [ 14K]  leafpart.f
│   │   └── [ 18K]  leafspring.f
│   ├── [4.0K]  LIBRARIES/
│   │   └── [ 65K]  seife.f
│   ├── [4.0K]  lincomb/
│   │   ├── [3.0K]  azimut12.f
│   │   ├── [3.3K]  azimut.f
│   │   ├── [ 342]  LINCOMB-renamed-linreg.txt
│   │   ├── [3.4K]  product.f
│   │   ├── [4.3K]  rectax.f
│   │   ├── [4.2K]  summe2.f
│   │   ├── [3.7K]  summe3.f
│   │   └── [4.2K]  triax.f
│   ├── [4.0K]  linregress/
│   │   ├── [4.0K]  data/
│   │   │   └── [ 149]  winplot.par
│   │   ├── [ 14K]  barocrosp.f
│   │   ├── [ 496]  baro.par
│   │   ├── [5.2K]  lincomb2.f
│   │   ├── [ 20K]  lincomb3.doc
│   │   ├── [6.6K]  lincomb3.f
│   │   ├── [ 178]  lincomb3x3.bat
│   │   ├── [5.2K]  linreg2.f
│   │   ├── [ 22K]  linreg3.doc
│   │   ├── [6.6K]  linreg3.f
│   │   ├── [ 175]  linreg3x3.bat
│   │   ├── [8.6K]  linreg6.f
│   │   ├── [ 29K]  orientation.doc
│   │   └── [ 16K]  remove.f
│   ├── [4.0K]  noisecon/
│   │   ├── [453K]  EX_4.1_rev1.doc
│   │   ├── [ 20K]  lownoise.doc
│   │   ├── [ 11K]  lownoise.f
│   │   ├── [ 314]  lownoise.par
│   │   ├── [2.9K]  noiseband.G32
│   │   ├── [9.4K]  noisecon.c
│   │   ├── [ 28K]  noisecon.doc
│   │   ├── [8.8K]  noisecon.f
│   │   └── [7.4K]  noisecon.G32
│   ├── [4.0K]  polzero/
│   │   ├── [ 23K]  Estimating_poles.doc
│   │   ├── [574K]  EX_5.5.doc
│   │   ├── [582K]  EX_5.5_rev1.doc
│   │   ├── [ 132]  frequencies.lst
│   │   ├── [3.1K]  perdamp.G32
│   │   ├── [ 28K]  polzero.doc
│   │   ├── [5.1K]  polzero.G32
│   │   ├── [ 38K]  Program_Description_PZFIT.doc
│   │   ├── [ 26K]  Program_Description_WINRESP.doc
│   │   ├── [ 24K]  pzfit.f
│   │   ├── [2.6K]  pzfit.par
│   │   ├── [9.2K]  pzplot.g32
│   │   ├── [ 32K]  Transforming-the-response.doc
│   │   ├── [  63]  winplot.par
│   │   ├── [ 26K]  WINRESP.doc
│   │   ├── [ 18K]  winresp.g32
│   │   ├── [4.8K]  xandemo.g32
│   │   └── [4.8K]  XANDEMO.G32
│   ├── [4.0K]  refseis/
│   │   ├── [4.0K]  earthmodels/
│   │   │   ├── [2.5K]  IBBN.par
│   │   │   ├── [2.8K]  MODELL.1
│   │   │   ├── [2.8K]  MODELL.1B1
│   │   │   ├── [2.8K]  MODELL.1B2
│   │   │   ├── [2.8K]  MODELL.1C1
│   │   │   ├── [2.8K]  MODELL.2
│   │   │   ├── [2.8K]  MODELL.2A1
│   │   │   ├── [2.8K]  MODELL.2A2
│   │   │   ├── [2.9K]  MODELL.3
│   │   │   ├── [2.9K]  MODELL.4
│   │   │   ├── [3.0K]  MODELL.5
│   │   │   ├── [2.9K]  MODELL.6
│   │   │   ├── [3.0K]  MODELL.7
│   │   │   └── [2.4K]  reftest.par
│   │   ├── [4.0K]  IBBN/
│   │   │   └── [2.5K]  IBBN.par
│   │   ├── [4.2K]  catfiles.f
│   │   ├── [4.3K]  enzfiles.f
│   │   ├── [3.8K]  razfiles.f
│   │   ├── [ 95K]  refseis.f
│   │   ├── [3.0K]  refseis.par
│   │   └── [ 31K]  refseis.txt
│   ├── [4.0K]  seife/
│   │   ├── [ 22K]  formats.doc
│   │   ├── [  56]  READ.ME
│   │   ├── [ 68K]  seife.doc
│   │   ├── [ 65K]  seife.f
│   │   ├── [ 129]  seife.par
│   │   ├── [  89]  SEIFE.PAR
│   │   └── [  55]  winplot.par
│   ├── [4.0K]  sincal/
│   │   ├── [ 39K]  sincal1.f
│   │   ├── [ 45K]  sincal2.f
│   │   ├── [ 48K]  sincal.doc
│   │   ├── [ 717]  sincal.par
│   │   ├── [ 33K]  sinfit.doc
│   │   ├── [6.7K]  sinfit.f
│   │   ├── [ 148]  wave.par
│   │   └── [ 195]  winplot.par
│   ├── [4.0K]  sleeman/
│   │   ├── [ 20K]  bandnois.doc
│   │   ├── [ 12K]  bandnois.f
│   │   ├── [ 141]  SEIFE.PAR
│   │   ├── [ 117]  sleeman.bat
│   │   ├── [ 43K]  sleeman.doc
│   │   ├── [5.1K]  sleeman.g32
│   │   ├── [5.6K]  sleeman_ia.g32
│   │   ├── [2.7K]  tri2db.f
│   │   ├── [ 13K]  tricrosp.f
│   │   ├── [ 34K]  twocrosp.doc
│   │   ├── [ 12K]  twocrosp.f
│   │   └── [  55]  winplot.par
│   ├── [4.0K]  spectral/
│   │   ├── [ 20K]  bandnois.doc
│   │   ├── [ 12K]  bandnois.f
│   │   ├── [ 112]  see-data-header.txt
│   │   ├── [ 24K]  seifft.doc
│   │   ├── [ 90K]  seifft.f
│   │   ├── [ 12K]  unispec.f
│   │   └── [ 152]  winplot.par
│   ├── [4.0K]  surfer/
│   │   └── [2.7K]  surfer.g32
│   ├── [4.0K]  testsig/
│   │   ├── [2.4K]  dbltestsig.f
│   │   ├── [2.4K]  gausstest.f
│   │   ├── [2.1K]  randtel.f
│   │   ├── [2.0K]  testsig2.f
│   │   ├── [2.0K]  testsig3.f
│   │   ├── [2.4K]  testsig.f
│   │   ├── [ 20K]  testsignals.doc
│   │   └── [ 20K]  Testsignals.doc
│   ├── [4.0K]  tides/
│   │   ├── [ 13K]  tides.f
│   │   ├── [ 133]  tides.par
│   │   └── [ 13K]  tidiff.f
│   ├── [4.0K]  tiltcal/
│   │   ├── [ 42K]  tiltcal.doc
│   │   ├── [ 37K]  tiltcal.f
│   │   ├── [1.1K]  tiltcal.par
│   │   └── [ 194]  winplot.par
│   ├── [4.0K]  vector-op/
│   │   ├── [3.0K]  azimut12.f
│   │   ├── [3.3K]  azimut.f
│   │   ├── [3.4K]  product.f
│   │   ├── [4.2K]  summe2.f
│   │   └── [3.7K]  summe3.f
│   └── [4.0K]  winplot/
│       ├── [6.0K]  colorplot.g32
│       ├── [5.9K]  Colorplot.g32
│       ├── [6.7K]  WinBoldPlot.g32
│       ├── [ 28K]  winplot.doc
│       ├── [7.0K]  Winplot.g32
│       ├── [6.6K]  WinPlot.g32
│       ├── [  72]  winplot.par
│       ├── [  71]  WINPLOT.PAR
│       ├── [6.1K]  XYplot.g32
│       ├── [ 19K]  zoomplot.doc
│       └── [ 12K]  zoomplot.g32
├── [ 23K]  CONTENTS-ls-lR.txt
├── [ 23K]  CONTENTS-tree.txt
├── [ 940]  COPYING
├── [ 32K]  LICENSE-GPLv3.txt
├── [3.0K]  README.md
└── [ 90K]  software-overview.doc

30 directories, 196 files
