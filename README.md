Software for Seismometry
========================

Purpose
-------
The repository contains software from the software subdirectory at
http://www.software-for-seismometry.de/.
Erhard Wielandt gave his consent to make his code available through gitlab for
anonymous download (see [COPYING](COPYING)).
The contents of the repository will be updated from time to time by vendor
updates.
Please check the original location (http://www.software-for-seismometry.de/)
for the most recent versions, binary executables for Windows and accompanying
material.
A UNIX/Linux-version of the software with a few add-ons is available at
https://gitlab.kit.edu/kit/gpi/bfo/seismology/software-for-seismometry-linux.

Source
------
Copied literally from http://www.software-for-seismometry.de/:

**Welcome to Erhard Wielandt's website for free seismological software**

This website offers free software for practical use, especially for the
design, calibration, and testing of seismographs. It also contains educational
software and other material used in courses and workshops on seismometry.
Problems of seismometry and theoretical seismology are treated in the 
'misc.texts'.

If you are interested in software, please start from the 
[software overview](software-overview.doc). 

[This website](http://www.software-for-seismometry.de/) is maintained by  
Prof. Erhard Wielandt (retired)  
Institute of Geophysics,  
Stuttgart University, Germany  
E-Mail: e.wielandt@t-online.de

Contents
--------
The git repository will contain only source code, examples of configuration
files, and text files containing program documentation.
All binary executables, binary libraries or tar-balls present at the source
location will not be included in the git repository.
Example data will be made available on a separate branch
`vendor-with-examples`.

Contents of the current vendor snapshot are presented in
 * [CONTENTS-tree.txt](CONTENTS-tree.txt)
 * [CONTENTS-ls-lR.txt](CONTENTS-ls-lR.txt)

Date of the snapshot: 14. May 2015

Layout
------
Source code is provided on branch `vendor`.

### branches
`vendor-preview`:        preview complete contents of
                         http://www.software-for-seismometry.de/
                         just before next vendor snapshot  
`vendor`:                vendor import branch (just code, no example data)   
`vendor-with-examples`:  vendor import with data file examples              
`master`:                is a child of `vendor` (additionally contains local
                         modifications and additions)
`master-with-examples`:  is a child of `vendor-with-examples` (additionally
                         contains local modifications and additions)
`workbench`:             separate branch providing tools for vendor imports   

### merge hierarchy
Branch `vendor-with-examples` never shall be merged into `vendor` such that
users can easily discard the example data files from their clone, if they want
to.
The branch `workbench` does not contain files from
http://www.software-for-seismometry.de/
and will always be kept separate (no merges).

Caretaker
---------
The git repository is maintained by  
Thomas Forbriger   
email: Thomas.Forbriger@kit.edu   
Black Forest Observatory (BFO), Heubach 206, 77709 Wolfach, Germany,   
Geophysical Institute (GPI), Karlsruhe Institute of Technology (KIT)   
https://www.gpi.kit.edu
